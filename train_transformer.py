#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
@Time    : 2021/3/3 6:11 下午
@Author  : W.D.
@File    : train.py
@Software: PyCharm
@Describe: 
'''
"""
Main file for training Yolo model on Pascal VOC dataset
"""

import torch
import torchvision.transforms as transforms
import torch.optim as optim
import torchvision.transforms.functional as FT
from tqdm import tqdm
from torch.utils.data import DataLoader
from model import Yolo_1D
from model import resnet34
from model import Encoder
from model import LocalEncoder
from dataset import Yolo_Dataset
from tools.utils import (
	non_maximum_suppression,
	mean_average_precision,
	intersection_over_union,
	cellboxes_to_boxes,
	get_bboxes,
	save_checkpoint,
	load_checkpoint,
)
from loss import Yolo_Loss_1D

seed = 2021
torch.manual_seed(seed)

# Hyperparameters etc.
LEARNING_RATE = 1e-3
DEVICE = "cuda" if torch.cuda.is_available() else "cpu"
BATCH_SIZE = 512  # 64 in original paper but I don't have that much vram, grad accum?
WEIGHT_DECAY = 5e-4
MOMENTUM = 0.949
EPOCHS = 1000
NUM_WORKERS = 2
PIN_MEMORY = True
LOAD_MODEL = False
LOAD_MODEL_FILE = "overfit_encoder.pth.tar"
S = 7
B = 2
C = 1
n_channel = 448
n_d = int(n_channel // S)

def train_fn(train_loader, model, optimizer, scheduler, loss_fn):
	loop = tqdm(train_loader, leave=True)
	mean_loss = []

	for batch_idx, (x, y) in enumerate(loop):
		x, y = x.to(DEVICE), y.to(DEVICE)
		out = model(x)
		# print(f'x shape:{x.shape}, out shape:{out.shape},y shape:{y.shape}')
		loss = loss_fn(out, y)
		mean_loss.append(loss.item())
		optimizer.zero_grad()
		loss.backward()
		optimizer.step()
		scheduler.step()


		# update progress bar
		loop.set_postfix(loss=loss.item())

	print(f'Mean loss was {sum(mean_loss) / len(mean_loss)}')


def main():
	print(DEVICE)
	# model = Yolo_1D(S=S, B=B, C=C).to(DEVICE)
	model = Encoder(n_d, 8, S, 6, DEVICE).to(DEVICE)
	optimizer = optim.Adam(model.parameters(), lr=LEARNING_RATE, weight_decay=WEIGHT_DECAY)
	scheduler = optim.lr_scheduler.CosineAnnealingLR(optimizer, 20)

	loss_fn = Yolo_Loss_1D(S=S, B=B, C=C)

	if LOAD_MODEL:
		load_checkpoint(torch.load(LOAD_MODEL_FILE), model, optimizer)

	train_dataset = Yolo_Dataset(txt_path=f'./data/train.txt', S=S, B=B, C=C)

	valid_dataset = Yolo_Dataset(txt_path=f'./data/valid.txt', S=S, B=B, C=C)

	train_loader = DataLoader(
		dataset=train_dataset,
		batch_size=BATCH_SIZE,
		num_workers=NUM_WORKERS,
		pin_memory=PIN_MEMORY,
		shuffle=True,
		drop_last=True,
	)

	valid_loader = DataLoader(
		dataset=valid_dataset,
		batch_size=BATCH_SIZE,
		num_workers=NUM_WORKERS,
		pin_memory=PIN_MEMORY,
		shuffle=False,
		drop_last=True,
	)

	best_mAP = 0.0
	for epoch in tqdm(range(EPOCHS)):
		pred_boxes, target_boxes = get_bboxes(valid_loader, model, iou_threshold=0.5, threshold=0.4, device=DEVICE)

		mean_avg_prec = mean_average_precision(pred_boxes, target_boxes, iou_threshold=0.5)
		print(f"Valid mAP: {mean_avg_prec}")

		if mean_avg_prec > best_mAP:
			best_mAP = mean_avg_prec
			checkpoint = {"state_dict": model.state_dict(), "optimizer": optimizer.state_dict()}
			save_checkpoint(checkpoint, filename=LOAD_MODEL_FILE)
			# import time
			# time.sleep(2)

		train_fn(train_loader, model, optimizer, scheduler,loss_fn)

if __name__ == "__main__":
	main()
