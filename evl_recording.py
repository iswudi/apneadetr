#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
@Time    : 2021/3/5 9:24 上午
@Author  : W.D.
@File    : evl_recording.py
@Software: PyCharm
@Describe: Main file for Apnea bbox detection.
'''
import torch
import numpy as np
import pandas as pd
from tqdm import tqdm
import time
import torch.optim as optim
import torchvision.transforms.functional as FT
from tqdm import tqdm
from torch.utils.data import DataLoader
from model import Yolo_1D
from model import resnet34
from dataset import Yolo_Dataset
import matplotlib.pyplot as plt
from tools.utils import (
	non_maximum_suppression,
	mean_average_precision,
	intersection_over_union,
	cellboxes_to_boxes,
	get_bboxes,
	plot_image,
	save_checkpoint,
	load_checkpoint,
)
from loss import Yolo_Loss_1D

seed = 2021
torch.manual_seed(seed)

# Hyperparameters etc.
LEARNING_RATE = 2e-5
DEVICE = "cuda" if torch.cuda.is_available() else "cpu"
BATCH_SIZE = 1  # 64 in original paper but I don't have that much vram, grad accum?
WEIGHT_DECAY = 0
EPOCHS = 500
NUM_WORKERS = 2
PIN_MEMORY = True
LOAD_MODEL = True
LOAD_MODEL_FILE = "overfit.pth.tar"

SAMPLE_RATE = 8  # Hz
WIN_SZ = 56  # second
OVERLAP = int(SAMPLE_RATE * WIN_SZ // 2)
S = 7
B = 2
C = 1


def get_prediction_boxes(sig_seg, model, iou_threshold, threshold):
	'''
	Return the predictions bounding boxes of given sig_seg.
	return nms_bboxes: list:[[c,p,x,w], ...]
	'''
	model.eval()
	with torch.no_grad():
		predictions = model(sig_seg)
		bboxes = cellboxes_to_boxes(predictions, S=S)
		nms_boxes = non_maximum_suppression(bboxes[0], iou_threshold=iou_threshold, threshold=threshold)
		return nms_boxes


def do_detect(signal, start_index, model, device):
	'''
	Evaluate a hole night respiratory signal and return a
	Dataframe format like: |sig_start|pred_start|pred_end|confidence|.

	:params: siganl: hole recording respiratory signal
	:params: start_index: index of each resp segments
	:params: model:
	:params: device:
	'''
	pred_df = []
	for sig_st in tqdm(start_index):
		sig_seg = signal[sig_st: sig_st + SAMPLE_RATE * WIN_SZ]
		# plt.plot(sig_seg)
		# plt.show()
		tensor_signal_seg = torch.tensor(sig_seg, dtype=torch.float32).unsqueeze(0).to(device)

		bboxe_preds = get_prediction_boxes(tensor_signal_seg, model, iou_threshold=0.8, threshold=0.4)
		for box in bboxe_preds:
			_, p, x, w = box
			confidence = p
			pred_start = sig_st + (x - w / 2) * SAMPLE_RATE * WIN_SZ
			pred_end = sig_st + (x + w / 2) * SAMPLE_RATE * WIN_SZ
			pred_df.append(
				{'SIG_START': sig_st, 'PRED_START': int(pred_start), 'PRED_END': int(pred_end), 'CONFIDENCE': confidence})
	return pd.DataFrame(pred_df)


def plot_anno(ax, anno, signal, vmin, vmax):
	'''

	'''
	for line in anno.itertuples():
		START, END = line.START, line.END
		ax.plot(np.arange(START, END) / SAMPLE_RATE, signal[START:END], 'r-')

		# plot arrow
		ax.annotate('', xy=(END / SAMPLE_RATE, vmin), xycoords='data', xytext=(START / SAMPLE_RATE, vmin),
					textcoords='data',
					arrowprops=dict(arrowstyle='->', connectionstyle='arc3'))
		# plot text
		arrow_h_offset = 0.05
		ax.text((START + END) / SAMPLE_RATE / 2, vmin + arrow_h_offset, f'Dur:{(END - START) / SAMPLE_RATE}s',
				ha='center', va='center',
				family='sans-serif')


def plot_pred(ax, pred, signal, vmin, vmax):
	'''

	'''
	for line in pred.itertuples():
		START, END, CONF = line.PRED_START, line.PRED_END, line.CONFIDENCE
		ax.plot(np.arange(START, END) / 8, signal[START:END], 'r-')
		# plot arrow
		ax.annotate('', xy=(END / SAMPLE_RATE, vmin), xycoords='data', xytext=(START / SAMPLE_RATE, vmin),
					textcoords='data', arrowprops=dict(arrowstyle='->', connectionstyle='arc3'))
		# plot text
		arrow_h_offset = 0.05
		ax.text((START + END) / SAMPLE_RATE / 2, vmin + arrow_h_offset, f'Dur:{(END - START) / SAMPLE_RATE:.1f}s',
				ha='center', va='center', family='sans-serif')
		ax.text((START + END) / SAMPLE_RATE / 2, vmin - arrow_h_offset, f'CONF:{CONF * 100:.2f}%', ha='center',
				va='center', family='sans-serif')


if __name__ == '__main__':
	model = resnet34(S=S, B=B, C=C).to(DEVICE)
	model.load_state_dict(torch.load(LOAD_MODEL_FILE, map_location='cpu')['state_dict'])
	t0 = time.time()
	psg = np.load('./ucd_npz/UCDDB002.npz', allow_pickle=True)
	resp = psg['ABDO'] + psg['RIBCAGE']
	resp = (resp - np.mean(resp)) / np.std(resp)

	anno_df = []
	for st, dur, _ in psg['ANNO']:
		anno_df.append({'START': int(st) * SAMPLE_RATE, 'END': (int(st) + int(dur)) * SAMPLE_RATE})
	anno_df = pd.DataFrame(anno_df)

	sig_st = list(range(0, len(resp) - SAMPLE_RATE * WIN_SZ, OVERLAP))
	pred_df = do_detect(resp, sig_st, model, DEVICE)
	show_cmp = True
	if show_cmp:
		# plot pred and anno events
		fig, axes = plt.subplots(ncols=1, nrows=2, sharex=True, sharey=True)

		axes[0].plot(np.arange((len(resp))) / SAMPLE_RATE, resp, label='APNEA-ANNO')
		axes[0].legend(loc='upper right')

		axes[1].plot(np.arange((len(resp))) / SAMPLE_RATE, resp, label='APNEA-PRED')
		axes[1].legend(loc='upper right')

		plot_anno(axes[0], anno_df, resp, np.min(resp), np.max(resp))
		plot_pred(axes[1], pred_df, resp, np.min(resp), np.max(resp))
		t1 = time.time()
		print(f'Predict 1 Record Used:{t1 - t0:.6f}s.')
		# plt.savefig(f'.{os.sep}cmp.png')
		plt.show()
