#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
@Time    : 2020/11/23 3:30 PM
@Author  : W.D.
@File    : create_train_dataset.py
@Software: PyCharm
@Describe: 构建YOLO1D 训练集和验证集
'''
import os
import time
from glob import glob

import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import minmax_scale
from tqdm import tqdm

from apnea_tools import pre_process_resp

np.random.seed(42)


class Create_Train_Dataset():
	def __init__(self, record_path):
		self.record_path = record_path
		self.train_record_pth, self.valid_record_pth = train_test_split(record_pth, test_size=0.2, random_state=42,
																		shuffle=True)
		self.TIME_WIN = 56

		with open(f'..{os.sep}data{os.sep}apnea.names', 'w') as f:
			f.write('Apnea')

		for pth in tqdm(self.train_record_pth, dynamic_ncols=True):
			self.generate_train_test_datasets(pth, True)

		for pth in tqdm(self.valid_record_pth, dynamic_ncols=True):
			self.generate_train_test_datasets(pth, False)

	def generate_train_test_datasets(self, path, train):
		print(path)
		self.record_name = os.path.splitext(os.path.basename(path))[0]
		self.record_idx = self.record_name[-3:]
		self.save_dir = os.path.join(f'..{os.sep}data', self.record_name)
		if not os.path.exists(self.save_dir):
			os.makedirs(self.save_dir)

		self.psg = np.load(path, allow_pickle=True)
		self.RESP_FS = self.psg['RIBCAGE_FS']
		self.sum = self.psg['ABDO'] + self.psg['RIBCAGE']
		self.sum = pre_process_resp(self.sum, self.RESP_FS)
		# self.signal = minmax_scale(self.sum, feature_range=(-1, 1))
		self.signal = (self.sum - np.mean(self.sum)) / np.std(self.sum)

		anno = self.psg['ANNO']
		annotation = []
		for st, dur, typ in anno:
			annotation.append({'START': int(st), 'END': int(st) + int(dur), 'TYPE': typ})
		self.annotation = pd.DataFrame(annotation)
		# self.annotation.to_csv(f'.{os.sep}{self.record_name}.csv', index=False)

		for self.line in self.annotation.itertuples():

			# data augment
			for d in ['center', 'left', 'right']:
				label_data = self.generate_random_bbox(direction=d)
				if train:
					txt_name = 'train.txt'
				else:
					txt_name = 'valid.txt'
				if label_data is not None:
					with open(f'..{os.sep}data{os.sep}{txt_name}', 'a') as f:
						f.write(label_data)
						f.close()

	def generate_random_bbox(self, direction='center'):
		'''
		For each apnea event, move random seconds to left/right.
		Return img_st, c, w, cls.
		:params: direction: 事件左右移动的方向
		'''

		# c = 0 if 'HYP' in line.TYPE else 1
		cls = 0
		width = self.line.END - self.line.START
		center = (self.line.END + self.line.START) // 2
		img_st = center - self.TIME_WIN // 2
		img_stp = center + self.TIME_WIN // 2

		try:
			step = np.random.randint(low=1, high=self.line.START - img_st)
		except Exception as e:
			print(f'{e}\nBounding box out of image size.\n')
			return None

		if direction == 'center':
			pass
		elif direction == 'left':
			img_st += step
			img_stp += step
		elif direction == 'right':
			img_st += (-1) * step
			img_stp += (-1) * step
		else:
			print('请输入正确的移动方向。\n')


		x1 = (self.line.START - img_st) / self.TIME_WIN

		x2 = (self.line.END - img_st) / self.TIME_WIN

		c = (x1 + x2) / 2

		w = x2 - x1

		# generate signal.npz
		saved_signal = self.signal[img_st * self.RESP_FS: img_stp * self.RESP_FS]

		save_path = f"{self.save_dir}{os.sep}{self.record_idx}{img_st * self.RESP_FS}.npz"
		np.savez(save_path, SUM=saved_signal, FS=self.RESP_FS)

		img_path = f'{self.record_name}{os.sep}{self.record_idx}{img_st * self.RESP_FS}.npz'

		label_data = f'{img_path} {cls},{c},{w}\n'
		return label_data


if __name__ == '__main__':
	t0 = time.time()
	record_pth = glob(f'..{os.sep}ucd_npz{os.sep}*.npz')
	ctd = Create_Train_Dataset(record_path=record_pth)
	t1 = time.time()
	print(f'Successfully create train and valid dataset used {t1 - t0:.2f} seconds!\n')
