# !/usr/bin/env python
# -*-coding:utf-8-*-
"""
@File: calc_std_and_mean.py
@Time: 2021/5/6 0006
@Author: WUDI
@Description:
"""
from glob import glob
import numpy as np
from tqdm import tqdm

if __name__ == '__main__':
    npz_list = glob(f'../data/*/*.npz')
    std_array, mean_array = [], []
    for path in tqdm(npz_list):
        resp = np.load(path, allow_pickle=True)['SUM']
        std_array.append(np.std(resp))
        mean_array.append(np.mean(resp))
    print(f'std:{np.mean(std_array)}, mean:{np.mean(mean_array)}')
    # std:0.8905973492299032, mean:-0.000130507509104628