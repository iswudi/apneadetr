#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
@Time    : 2021/1/28 2:22 PM
@Author  : W.D.
@File    : utils.py
@Software: PyCharm
@Describe: 
'''
import torch
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as patches
from collections import Counter

ellipsis = 1e-6


def intersection_over_union(boxes_preds, boxes_labels):
	"""
	Calculates intersection over union
	Parameters:
		boxes_preds (tensor): Predictions of Bounding Boxes (BATCH_SIZE, 2)
		boxes_labels (tensor): Correct labels of Bounding Boxes (BATCH_SIZE, 2)
		box_format (str): (x,y)
	Returns:
		tensor: Intersection over union for all examples
	"""
	# print(boxes_preds.shape)
	# print(boxes_labels.shape)
	# boxes format is midpoint
	box1_x = boxes_preds[..., 0] - boxes_preds[..., 1] / 2
	box1_y = boxes_preds[..., 0] + boxes_preds[..., 1] / 2

	box2_x = boxes_labels[..., 0] - boxes_labels[..., 1] / 2
	box2_y = boxes_labels[..., 0] + boxes_labels[..., 1] / 2

	x1 = torch.min(box1_x, box2_x)
	x2 = torch.max(box1_x, box2_x)
	y1 = torch.min(box1_y, box2_y)
	y2 = torch.max(box1_y, box2_y)

	intersection = (y1 - x2).clamp(0)
	union = y2 - x1
	return intersection / union


def non_maximum_suppression(bboxes, iou_threshold, threshold=0.5):
	"""
	Calculate non maximum supression.
	Parameters:
		boxes (tensor): Predictions of Bounding Boxes [[cls, conf, x, y],[]...]
	"""
	assert type(bboxes) == list

	bboxes = [bbox for bbox in bboxes if bbox[1] > threshold]
	bboxes = sorted(bboxes, key=lambda x: x[1], reverse=True)
	bboxes_after_nms = []

	while bboxes:
		chosen_box = bboxes.pop(0)

		# 需要保留的边框
		bboxes = [
			box for box in bboxes if box[0] != chosen_box[0]
									 or intersection_over_union(torch.tensor(chosen_box[2:]), torch.tensor(box[2:])) < iou_threshold
		]

		bboxes_after_nms.append(chosen_box)
	return bboxes_after_nms


def mean_average_precision(pred_boxes, true_boxes, iou_threshold=0.5, num_classes=1):
	"""
	Parameters:
		pred_boxes (list): list of lists containing all bboxes with each bboxes
		specified as [train_idx, class_prediction, prob_score, x, y]
		true_boxes (list): Similar as pred_boxes except all the correct ones
		iou_threshold (float): threshold where predicted bboxes is correct
		box_format (str): （x,y)
		num_classes (int): number of classes
	"""
	assert type(pred_boxes) == list

	# 保存每个类的AP
	average_precision = []

	# 防止除0
	epsilon = 1e-6

	for cls in range(num_classes):
		# 保留该类别的所有边框
		detections = []
		ground_truths = []

		for detection in pred_boxes:
			if detection[1] == cls:
				detections.append(detection)

		for true_box in true_boxes:
			if true_box[1] == cls:
				ground_truths.append(true_box)

		# 统计每个image_id包含边框的数量
		# 用于记录是否检测过某个边框
		amount_boxes = Counter([gt[0] for gt in ground_truths])

		for k, v in amount_boxes.items():
			amount_boxes[k] = torch.zeros(v)

		# 按置信度降序排列
		detections.sort(key=lambda x: x[2], reverse=True)
		TP = torch.zeros(len(detections))
		FP = torch.zeros(len(detections))
		total_true_bboxes = len(ground_truths)

		if total_true_bboxes == 0:
			continue

		for detection_idx, detection in enumerate(detections):

			# 只选择与预测边框image_id相同的ground truth boxes
			ground_truth_img = [box for box in ground_truths if box[0] == detection[0]]

			num_gt = len(ground_truth_img)
			best_iou = 0

			for idx, gt in enumerate(ground_truth_img):
				iou = intersection_over_union(torch.tensor(detection[3:]), torch.tensor(gt[3:]))

				if iou > best_iou:
					best_iou = iou
					best_gt_idx = idx

			if best_iou > iou_threshold:
				# 每个边框只检测一个结果
				if amount_boxes[detection[0]][best_gt_idx] == 0:
					TP[detection_idx] = 1
					amount_boxes[detection[0]][best_gt_idx] = 1
				else:
					FP[detection_idx] = 1
			else:
				FP[detection_idx] = 1

		TP_cumsum = torch.cumsum(TP, dim=0)
		FP_cumsum = torch.cumsum(FP, dim=0)

		precisions = TP_cumsum / (TP_cumsum + FP_cumsum + ellipsis)
		recalls = TP_cumsum / (total_true_bboxes + ellipsis)
		precisions = torch.cat((torch.tensor([1]), precisions))
		recalls = torch.cat((torch.tensor([0]), recalls))

		average_precision.append(torch.trapz(precisions, recalls))

		return sum(average_precision) / len(average_precision)


def plot_image(boxes, type, axes):
	"""Plots predicted bounding boxes on the image"""
	width, height = 448, 4
	color_dict = {'label': 'r', 'pred': 'g'}
	# Create a Rectangle potch
	for box in boxes:
		box = box[2:]
		assert len(box) == 2, "Got more values than in x, w in a box!"
		upper_left_x = box[0] - box[1] / 2
		upper_left_y = -0.5
		rect = patches.Rectangle(
			(upper_left_x * width, upper_left_y * height),
			box[1] * width,
			height,
			linewidth=1,
			edgecolor=color_dict.get(type),
			facecolor="none",
		)
		# Add the patch to the Axes
		axes.add_patch(rect)


# def plot_image(signal, boxes):
# 	"""Plots predicted bounding boxes on the image"""
#
# 	# Create figure and axes
# 	fig, ax = plt.subplots(1)
# 	# Display the image
# 	ax.plot(signal)
#
# 	# Create a Rectangle potch
# 	for box in boxes:
# 		box = box[2:]
# 		assert len(box) == 2, "Got more values than in x, y, w, h, in a box!"
# 		upper_left_x = box[0] - box[1] / 2
# 		start = box[0] - box[1] / 2
# 		upper_left_y = box[1] - box[3] / 2
# 		end = box[0] + box[1] / 2
# 		rect = patches.Rectangle(
# 			(start * width, upper_left_y * height),
# 			box[2] * width,
# 			box[3] * height,
# 			linewidth=1,
# 			edgecolor="r",
# 			facecolor="none",
# 		)
# 		# Add the patch to the Axes
# 		ax.add_patch(rect)
#
# 	plt.show()


def get_bboxes(loader, model, iou_threshold, threshold, device):
	all_pred_boxes = []
	all_true_boxes = []

	# make sure model is in eval before get bboxes
	model.eval()
	train_idx = 0

	for batch_idx, (x, labels) in enumerate(loader):
		x = x.to(device)
		labels = labels.to(device)

		with torch.no_grad():
			predictions = model(x)

		batch_size = x.shape[0]
		true_bboxes = cellboxes_to_boxes(labels)
		bboxes = cellboxes_to_boxes(predictions)

		for idx in range(batch_size):
			nms_boxes = non_maximum_suppression(
				bboxes[idx],
				iou_threshold=iou_threshold,
				threshold=threshold,
			)

			# if batch_idx == 0 and idx == 0:
			#    plot_image(x[idx].permute(1,2,0).to("cpu"), nms_boxes)
			#    print(nms_boxes)

			for nms_box in nms_boxes:
				all_pred_boxes.append([train_idx] + nms_box)

			for box in true_bboxes[idx]:
				# many will get converted to 0 pred
				if box[1] > threshold:
					all_true_boxes.append([train_idx] + box)

			train_idx += 1

	model.train()
	return all_pred_boxes, all_true_boxes


def convert_cellboxes(predictions, S=7):
	"""
	Converts bounding boxes output from Yolo with
	an image split size of S into entire image ratios
	rather than relative to cell ratios. Tried to do this
	vectorized, but this resulted in quite difficult to read
	code... Use as a black box? Or implement a more intuitive,
	using 2 for loops iterating range(S) and convert them one
	by one, resulting in a slower but more readable implementation.
	"""

	predictions = predictions.to("cpu")
	batch_size = predictions.shape[0]
	predictions = predictions.reshape(batch_size, 7, 7)
	bboxes1 = predictions[..., 2:4]
	bboxes2 = predictions[..., 5:7]
	scores = torch.cat((predictions[..., 1].unsqueeze(0), predictions[..., 4].unsqueeze(0)), dim=0)
	best_box = scores.argmax(0).unsqueeze(-1)
	best_boxes = bboxes1 * (1 - best_box) + best_box * bboxes2
	cell_indices = torch.arange(7).repeat(batch_size, 1).unsqueeze(-1)
	x = 1 / S * (best_boxes[..., :1] + cell_indices)
	w = 1 / S * best_boxes[..., 1:2]
	converted_bboxes = torch.cat((x, w), dim=-1)
	predicted_class = predictions[..., :1].argmax(-1).unsqueeze(-1)
	predicted_class = torch.tensor(predicted_class, dtype=torch.float32)
	best_confidence = torch.max(predictions[..., 1], predictions[..., 4]).unsqueeze(-1)
	converted_preds = torch.cat((predicted_class, best_confidence, converted_bboxes), dim=-1)

	return converted_preds


def cellboxes_to_boxes(out, S=7):
	converted_pred = convert_cellboxes(out).reshape(out.shape[0], S, -1)
	converted_pred[..., 0] = converted_pred[..., 0].long()
	all_bboxes = []

	for ex_idx in range(out.shape[0]):
		bboxes = []

		for bbox_idx in range(S):
			bboxes.append([x.item() for x in converted_pred[ex_idx, bbox_idx, :]])
		all_bboxes.append(bboxes)

	return all_bboxes


def save_checkpoint(state, filename="my_checkpoint.pth.tar"):
	print("=> Saving checkpoint")
	torch.save(state, filename)


def load_checkpoint(checkpoint, model, optimizer):
	print("=> Loading checkpoint")
	model.load_state_dict(checkpoint["state_dict"])
	optimizer.load_state_dict(checkpoint["optimizer"])


if __name__ == '__main__':
	x = torch.randn(4, 2)
	y = torch.randn(4, 2)
	iou = intersection_over_union(x, y)
