#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
@Time    : 2020/10/14 3:22 PM
@Author  : W.D.
@File    : apnea_tools.py
@Software: PyCharm
@Describe: 
'''
import pywt
import numpy as np
import matplotlib.pyplot as plt
from scipy import interpolate
from scipy import signal
from scipy.signal import filtfilt, butter

def pre_process_resp(x, sampling_rate, show=False):
	'''

	:param x: 原始信号
	:param sampling_rate: 原始信号采样频率
	:param show: 是否检视图
	:return: 处理后的信号
	'''
	# 1. remove the spark
	s1 = spark_resp(x)
	y = spark_resp(s1)

	# 2. remove BW by wavelet
	level = 7
	wavelet_str = 'haar'
	coeffs = pywt.wavedec(y, wavelet_str, level=level)
	bw = wrcoef(y, 'a', coeffs, wavelet_str, level)
	z = y - bw

	# 3. median filter
	# z1 = signal.medfilt(z, 15)

	# 4. return the filter signal
	order = 4
	frq = np.asarray([0.1, 0.5])
	filtered = filter_signal(signal=z, band='bandpass', order=order, frequency=frq, sampling_rate=sampling_rate)

	if show:
		plt.figure()

		ax1 = plt.subplot(2, 1, 1)
		plt.plot(x, label='raw data')
		plt.plot(y, label='remove spark')
		# plt.legend()

		ax2 = plt.subplot(2, 1, 2, sharex=ax1)
		plt.plot(z, label='remove BW')
		plt.plot(filtered, label='filtered signal')
		# plt.legend()
		plt.show()

	filtered_sig = filtered - np.mean(filtered)
	return filtered_sig

def spark_resp(x):
	'''

	:param x: 输入原始信号
	:return: 处理后的平滑信号
	'''
	x = x - np.mean(x)
	x_diff = np.diff(x)
	threshold = np.mean(x_diff) + 5 * np.std(x_diff)
	idx = np.where(x_diff > threshold)[0] # the return type is tuple
	width = 15
	single_side = int((width - 1) / 2)
	for i in range(len(idx)):
		st = idx[i] - single_side
		stp = idx[i] + single_side + 1
		if st < 0:
			st = 0
		if stp > len(x) - 1:
			stp = len(x) - 1
		win = x[st:stp]
		x[idx[i]] = np.median(win)
	x_smoothed = x
	return x_smoothed

def wrcoef(x, coef_type, coefficient, wave_name, level):
	n = np.array(x).size
	a, ds = coefficient[0], list(reversed(coefficient[1:]))

	if coef_type == 'a':
		return pywt.upcoef('a', a, wave_name, level=level)[:n]
	elif coef_type == 'd':
		return pywt.upcoef('d', ds[level-1], wave_name, level=level)[:n]
	else:
		raise ValueError('Invalid coefficient type:{}'.format(coef_type))

def filter_signal(signal=None, band='lowpass', order=None, frequency=None, sampling_rate=8):
	'''

	:param signal:
	:param band:
	:param order:
	:param frequency:
	:param sampling_rate:
	:return:
	'''

	# check inputs
	if signal is None:
		raise TypeError('Please specify a signal to filter.')

	fs = float(sampling_rate)
	wn = (2. * frequency) / fs
	b, a = butter(order, wn, btype=band, analog=False, output='ba')

	# filter
	filtered = filtfilt(b, a, signal)
	return filtered