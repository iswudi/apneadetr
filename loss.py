#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
@Time    : 2021/2/2 7:08 PM
@Author  : W.D.
@File    : loss.py
@Software: PyCharm
@Describe: 
'''
import torch
import torch.nn as nn
from tools.utils import intersection_over_union


class Yolo_Loss_1D(nn.Module):
	def __init__(self, S=7, B=2, C=1):
		super(Yolo_Loss_1D, self).__init__()
		self.S = S
		self.B = B
		self.C = C
		self.mse = nn.MSELoss(reduction='sum')
		self.lambda_coord = 5
		self.lambda_nobj = 0.5

	def forward(self, pred_bboxes, target_bboxes):
		# 目标 shape [N, S, C + 1 + 2 ]
		# in 1-D version [N, S, 4]  [cls,p,x,w]

		pred_bboxes = pred_bboxes.reshape(-1, self.S, self.C + self.B * (2 + 1))
		# print(f'pred_bboxes shape:{pred_bboxes.shape}, target_bboxes shape:{target_bboxes.shape}')
		# 每个cell [cls,p,x,w,p,x,w]
		iou_b1 = intersection_over_union(pred_bboxes[..., 2:4], target_bboxes[..., 2:4]).unsqueeze(2)
		iou_b2 = intersection_over_union(pred_bboxes[..., 5:7], target_bboxes[..., 2:4]).unsqueeze(2)
		# print(f'iou_b1 shape:{iou_b1.shape}, iou_b2 shape:{iou_b2.shape}')
		ious = torch.cat([iou_b1.unsqueeze(0), iou_b2.unsqueeze(0)], dim=0)

		# 选取iou最大的那个边框
		iou_maxes, bestbox = torch.max(ious, dim=0)

		# 存在目标边框的标识
		exist_bboxes = target_bboxes[..., 1].unsqueeze(2)

		# bboxes loss
		# 预测边框为存在目标的并且iou最大的那个cell计算loss
		# shape [N, S, 1 + 1]
		bboxes_predictions = exist_bboxes * ((1 - bestbox) * pred_bboxes[..., 2:4] + bestbox * pred_bboxes[..., 5:7])
		bboxes_targets = exist_bboxes * target_bboxes[..., 2:4]

		# sqrt of w
		bboxes_predictions[..., 1] = torch.sign(bboxes_predictions[..., 1]) * torch.sqrt(
			torch.abs(bboxes_predictions[..., 1] + 1e-6))
		bboxes_targets[..., 1] = torch.sqrt(bboxes_targets[..., 1])

		boxes_loss = self.mse(
			torch.flatten(bboxes_predictions, end_dim=-2),
			torch.flatten(bboxes_targets, end_dim=-2)
		)

		# classes loss
		class_loss = self.mse(
			torch.flatten(exist_bboxes * pred_bboxes[..., :1]),
			torch.flatten(exist_bboxes * target_bboxes[..., :1])
		)

		# obj-loss
		obj_loss = self.mse(
			torch.flatten(exist_bboxes * ((1 - bestbox) * pred_bboxes[..., 1:2] + bestbox * pred_bboxes[..., 4:5])),
			torch.flatten(exist_bboxes * target_bboxes[..., 1:2])
		)

		# no-obj loss
		noobj_loss = self.mse(
			torch.flatten((1 - exist_bboxes) * pred_bboxes[..., 1:2], start_dim=1),
			torch.flatten((1 - exist_bboxes) * target_bboxes[..., 1:2], start_dim=1)
		)
		noobj_loss += self.mse(
			torch.flatten((1 - exist_bboxes) * pred_bboxes[..., 4:5], start_dim=1),
			torch.flatten((1 - exist_bboxes) * target_bboxes[..., 1:2], start_dim=1)
		)

		loss = (
			self.lambda_coord * boxes_loss
			+ obj_loss
			+ noobj_loss
			+ class_loss
		)
		return loss


if __name__ == '__main__':
	x = torch.randn(20, 7, 7)
	y = torch.randn(20, 7, 4)
	func = Yolo_loss_1d()
	loss = func(x, y)
	print(loss)
