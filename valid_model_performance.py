#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
@Time    : 2021/3/5 9:24 上午
@Author  : W.D.
@File    : valid_model_performance.py
@Software: PyCharm
@Describe: Main file for evaluate model performance.
'''
import torch
import torch.optim as optim
import torchvision.transforms.functional as FT
from tqdm import tqdm
from torch.utils.data import DataLoader
from model import Yolo_1D
from model import resnet34
from dataset import Yolo_Dataset
import matplotlib.pyplot as plt
import seaborn as sns
from tools.utils import (
    non_maximum_suppression,
    mean_average_precision,
    intersection_over_union,
    cellboxes_to_boxes,
    get_bboxes,
    plot_image,
    save_checkpoint,
    load_checkpoint,
)
from loss import Yolo_Loss_1D

sns.set(context='paper', style='whitegrid', palette='muted', font_scale=1)
HAPPY_COLORS_PALETTE = ["#01BEFE", "#FFDD00", "#FF7D00", "#FF006D", "#ADFF02", "#8F00FF"]
sns.set_palette(sns.color_palette(HAPPY_COLORS_PALETTE))
# plt.rcParams['font.sans-serif'] = ['SimHei']  # 解决中文显示问题-设置字体为黑体
# plt.rcParams['axes.unicode_minus'] = False

seed = 2021
torch.manual_seed(seed)

# Hyperparameters etc.
LEARNING_RATE = 2e-5
DEVICE = "cuda" if torch.cuda.is_available() else "cpu"
BATCH_SIZE = 1  # 64 in original paper but I don't have that much vram, grad accum?
WEIGHT_DECAY = 0
EPOCHS = 500
NUM_WORKERS = 2
PIN_MEMORY = True
LOAD_MODEL = True
LOAD_MODEL_FILE = "overfit.pth.tar"
S = 7
B = 2
C = 1


def detet_fn(loader, model, iou_threshold, threshold, device):
    # make sure model is in eval before get bboxes
    model.eval()
    train_idx = 0
    for x, labels in (loader):
        x = x.to(device)
        labels = labels.to(device)

        with torch.no_grad():
            predictions = model(x)

        true_bboxes = cellboxes_to_boxes(labels)
        bboxes = cellboxes_to_boxes(predictions)

        nms_boxes = non_maximum_suppression(bboxes[0], iou_threshold=iou_threshold, threshold=threshold)

        true_bboxes = [boxes for boxes in true_bboxes[0] if boxes[1] > threshold]

        # Create figure and axes
        fig, axes = plt.subplots(figsize=(4, 4), dpi=112)
        axes.set_xlim(0, 448)
        # axes.set_ylim(-2, 2)
        # Display the image
        axes.plot(x.squeeze(dim=0).cpu())
        plot_image(nms_boxes, 'pred', axes)
        plot_image(true_bboxes, 'label', axes)
        plt.show()


if __name__ == '__main__':
    model = resnet34(S=S, B=B, C=C)
    optimizer = optim.Adam(model.parameters(), lr=LEARNING_RATE, weight_decay=WEIGHT_DECAY)

    if LOAD_MODEL:
        load_checkpoint(torch.load(LOAD_MODEL_FILE, map_location='cpu'), model, optimizer)

    model.to(DEVICE)

    train_dataset = Yolo_Dataset(txt_path=f'./data/train.txt', S=S, B=B, C=C)
    train_loader = DataLoader(
        dataset=train_dataset,
        batch_size=BATCH_SIZE,
        num_workers=NUM_WORKERS,
        pin_memory=PIN_MEMORY,
        shuffle=True,
        drop_last=True,
    )

    valid_dataset = Yolo_Dataset(txt_path=f'./data/valid.txt', S=S, B=B, C=C)
    valid_loader = DataLoader(
        dataset=valid_dataset,
        batch_size=BATCH_SIZE,
        num_workers=NUM_WORKERS,
        pin_memory=PIN_MEMORY,
        shuffle=False,
        drop_last=True,
    )
    detet_fn(train_loader, model, iou_threshold=0.5, threshold=0.4, device=DEVICE)
