# *ApneaDetr* : Data driven YOLO based sleep apnea localization model

- Paper Yolo-v1:
  - [ ] Train
  - [ ] Evaluation

# 0. 下载预训练模型
可选择的预训练模型(由于部分模型较大，并未上传到本仓库内，均在U盘内有备份)
- [x] overfit_yolo1d.pth.tar
- [x] overfit_resnet18.pth.tar
- [x] overfit_resnet34.pth.tar
- [x] overfit_resnet152.pth.tar
- [x] overfit_encoder.pth.tar
- [x] overfit_transformer.pth.tar
- [x] overfit_local_encoder.pth.tar


# 1. 流程图
![flowchart](./imgs/yolo_1d.png) 

# 2. 构建训练数据集&数据集增强
![build train dataset](./imgs/build_dataset.png) 

![dataset augment](./imgs/dataset_augment.png) 

# 3. 训练
```python
# 注意替换模型及路径
python train.py
```

# 4. 睡眠呼吸事件检测
```python
python valid_model_performance.py
```
| 参数          | 说明           |
|---------------|----------------|
| iou_threshold | 非极大抑制阈值 |
| threshold     | 置信度阈值     |



# 补充实验
- 数据集增强前后性能对比
- loss函数修改测试
- 本地数据泛化？
