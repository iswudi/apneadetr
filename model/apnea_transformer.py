#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
@Time    : 2021/3/9 9:39 上午
@Author  : W.D.
@File    : apnea_transformer.py
@Software: PyCharm
@Describe: 
'''
import torch
import math
import torch.nn as nn
import torch.nn.functional as F
import matplotlib.pyplot as plt


class Position_Embedding(nn.Module):
	def __init__(self, d_model, L, temperature=1e4):
		'''
		d_model: dimension of input vector
		L: number of vectors
		'''
		super(Position_Embedding, self).__init__()
		self.d_model = d_model
		self.L = L
		self.temperature = temperature
		self.pe_matrix = self.get_pe_matrix

	@property
	def get_pe_matrix(self):
		pe_matrix = torch.zeros(self.L, self.d_model)
		for pos in range(0, self.L):
			for i in range(0, self.d_model, 2):
				pe_matrix[pos, i] = math.sin(pos / (self.temperature ** ((2 * i) / self.d_model)))
				pe_matrix[pos, i + 1] = math.cos(pos / (self.temperature ** ((2 * i) / self.d_model)))
		return pe_matrix

	def forward(self, positions):
		return self.pe_matrix[positions]


class Attention(nn.Module):
	def __init__(self):
		super(Attention, self).__init__()
		self.dropout = nn.Dropout(0.1)

	def forward(self, q, k, v):
		'''
		q, k, v : (N, L, d_model)
		'''
		d_model = q.shape[-1]
		scores = torch.matmul(q, k.transpose(-1, -2)) / math.sqrt(d_model)
		scores = torch.softmax(self.dropout(scores), dim=-1)
		attention = torch.matmul(scores, v)
		return attention


class Multi_Head_Attention(nn.Module):
	def __init__(self, d_model, heads, ):
		super(Multi_Head_Attention, self).__init__()
		self.d_model = d_model
		self.heads = heads
		self.d_k = d_model // heads
		self.attention = Attention()
		self.queries = nn.Linear(d_model, d_model)
		self.keys = nn.Linear(d_model, d_model)
		self.values = nn.Linear(d_model, d_model)
		self.linear = nn.Linear(d_model, d_model)

	def forward(self, query, key, value):
		N, L, d_model = query.shape[0], query.shape[1], query.shape[2]

		query = self.queries(query).reshape(N, -1, self.heads, self.d_k)
		key = self.queries(key).reshape(N, -1, self.heads, self.d_k)
		value = self.queries(value).reshape(N, -1, self.heads, self.d_k)

		query = query.transpose(1, 2)
		key = key.transpose(1, 2)
		value = value.transpose(1, 2)
		attention = self.attention(query, key, value).transpose(1, 2).reshape(N, -1, d_model)
		out = self.linear(attention)
		return out

class FeedForward(nn.Module):
	def __init__(self, d_model=512, dff=2048, dropout=0.1):
		super(FeedForward, self).__init__()
		self.linear_1 = nn.Linear(d_model, dff)
		self.linear_2 = nn.Linear(dff, d_model)
		self.dropout = nn.Dropout(dropout)
		self.relu = nn.ReLU()

	def forward(self, x):
		x = self.dropout(F.relu(self.linear_1(x)))
		return self.linear_2(x)

class EncoderLayer(nn.Module):
	def __init__(self, d_model=512, dropout=0.1):
		super(EncoderLayer, self).__init__()
		self.multi_head_attention = Multi_Head_Attention(d_model=d_model, heads=8)
		self.fnn = FeedForward(d_model)
		self.layernorm1 = nn.LayerNorm(d_model)
		self.layernorm2 = nn.LayerNorm(d_model)
		self.dropout = nn.Dropout(dropout)

	def forward(self, q, k, v):
		x = q
		out = self.layernorm1(x + self.multi_head_attention(q, k, v))
		out = self.layernorm2(out + self.fnn(out))
		return self.dropout(out)

class Encoder(nn.Module):
	def __init__(self, d_model, heads, L, num_layers, device, dropout=0.1):
		super(Encoder, self).__init__()
		self.d_model = d_model
		self.heads = heads
		self.L = L
		self.num_layers = num_layers
		self.device = device
		self.pe = Position_Embedding(d_model, L)
		self.encoder_layers = nn.ModuleList([EncoderLayer(d_model) for _ in range(num_layers)])
		self.dropout = nn.Dropout(dropout)
		C = 1
		B = 2
		self.pred_heads = nn.Linear(d_model, C + B * (2 + 1))
	def forward(self, x):
		N = x.shape[0]
		x = x.reshape(N, self.L, -1)
		positions = torch.arange(0, self.L).expand(N, self.L)
		x = x + self.pe(positions).to(self.device)
		x = self.dropout(x)

		for layer in self.encoder_layers:
			x = layer(x, x, x)
		out = self.pred_heads(x).reshape(N, -1)
		return out


if __name__ == '__main__':
	x = torch.randn(16, 448).reshape(16, 7, -1)

	encoder = Encoder(64, 8, 7, 6, 'cpu')
	out = encoder(x)
	print(out.shape)
