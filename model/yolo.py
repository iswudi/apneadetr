#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
@Time    : 2021/2/1 3:27 PM
@Author  : W.D.
@File    : yolo.py
@Software: PyCharm
@Describe: 
'''
import torch
import torch.nn as nn

cfg = [
	(7, 64, 2, 3),
	'M',
	(3, 192, 1, 1),
	'M',
	(1, 128, 1, 0),
	(3, 256, 1, 1),
	(1, 256, 1, 0),
	(3, 512, 1, 1),
	'M',
	[(1, 256, 1, 0), (3, 512, 1, 1), 4],
	(1, 512, 1, 0),
	(3, 1024, 1, 1),
	'M',
	[(1, 512, 1, 0), (3, 1024, 1, 1), 2],
	(3, 1024, 1, 1),
	(3, 1024, 2, 1),
	(3, 1024, 1, 1),
	(3, 1024, 1, 1),
]


class ConvBlock(nn.Module):
	def __init__(self, in_channels, out_channels, **kwargs):
		super(ConvBlock, self).__init__()
		self.conv = nn.Conv1d(in_channels, out_channels, bias=False, **kwargs)
		self.batchnorm = nn.BatchNorm1d(out_channels)
		self.leakyrelu = nn.LeakyReLU(0.1)

	def forward(self, x):
		return self.leakyrelu(self.batchnorm(self.conv(x)))


class Yolo_1D(nn.Module):
	def __init__(self, in_channels=1, **kwargs):
		super(Yolo_1D, self).__init__()
		self.in_channels = in_channels
		self.darknet = self._make_conv_layer(cfg)
		self.fc = self._make_fc_layer(**kwargs)

	def forward(self, x):
		x = x.unsqueeze(dim=1)
		x = self.darknet(x)
		return self.fc(torch.flatten(x, start_dim=1))

	def _make_conv_layer(self, cfg):
		layers = []
		for blocks in cfg:
			if type(blocks) == tuple:
				layers.append(
					ConvBlock(self.in_channels, blocks[1], kernel_size=blocks[0], stride=blocks[2], padding=blocks[3]))
				self.in_channels = blocks[1]
			if type(blocks) == str:
				layers.append(nn.MaxPool1d(2, 2))
			if type(blocks) == list:
				conv1 = blocks[0]
				conv2 = blocks[1]
				for i in range(blocks[-1]):
					layers.append(ConvBlock(self.in_channels, conv1[1], kernel_size=conv1[0], stride=conv1[2],
											padding=conv1[3]))
					layers.append(ConvBlock(conv1[1], conv2[1], kernel_size=conv2[0], stride=conv2[2],
											padding=conv2[3]))
					self.in_channels = conv2[1]
		return nn.Sequential(*layers)

	def _make_fc_layer(self, S=7, B=2, C=1):

		return nn.Sequential(
			nn.Linear(S * 1024, 496),
			nn.Dropout(0.5),
			nn.LeakyReLU(),
			nn.Linear(496, S * (B * 3 + C))
		)


if __name__ == '__main__':
	model = Yolo_1D()
	x = torch.randn(4, 1, 448)
	out = model(x)
	print(out.shape)
