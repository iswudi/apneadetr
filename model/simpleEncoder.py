# !/usr/bin/env python
# -*-coding:utf-8-*-
"""
@File: simpleEncoder.py
@Time: 2021/5/6 0006
@Author: WUDI
@Description:
"""
import torch
import torch.nn as nn
import math


class Position_Embedding(nn.Module):
    def __init__(self, d_model=64, L=7, temperature=1e4):
        '''
        d_model: dimension of input vector
        L: number of vectors
        '''
        super(Position_Embedding, self).__init__()
        self.d_model = d_model
        self.L = L
        self.temperature = temperature
        self.pe_matrix = self.get_pe_matrix

    @property
    def get_pe_matrix(self):
        pe_matrix = torch.zeros(self.L, self.d_model)
        for pos in range(0, self.L):
            for i in range(0, self.d_model, 2):
                pe_matrix[pos, i] = math.sin(pos / (self.temperature ** ((2 * i) / self.d_model)))
                pe_matrix[pos, i + 1] = math.cos(pos / (self.temperature ** ((2 * i) / self.d_model)))
        return pe_matrix

    def forward(self, positions):
        return self.pe_matrix[positions]


class SimpleEncoder(nn.Module):
    def __init__(self, d_model=64, num_heads=8, num_enc_layers=6, device='cuda'):
        super(SimpleEncoder, self).__init__()
        self.d_model = d_model
        self.num_heads = num_heads
        self.num_enc_layers = num_enc_layers
        self.device = device
        self.pe = Position_Embedding()
        self.encoder_layer = nn.TransformerEncoderLayer(d_model=self.d_model, nhead=self.num_heads)
        self.layer_norm = nn.LayerNorm(self.d_model)
        self.encoder = nn.TransformerEncoder(self.encoder_layer, self.num_enc_layers, self.layer_norm)
        self.to_cell = nn.Linear(self.d_model, 7)

    def forward(self, x):
        N = x.shape[0]
        x = x.reshape(N, 7, -1)
        positions = torch.arange(0, 7).expand(N, 7)
        x = x + self.pe(positions).to(self.device)
        out = self.to_cell(self.encoder(x)).reshape(N, -1)
        return out


if __name__ == '__main__':
    x = torch.randn(16, 448).reshape(16, 7, -1)
    encoder = SimpleEncoder()
    y = encoder(x)
    print(y.shape)
