#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
@Time    : 2021/2/3 4:18 PM
@Author  : W.D.
@File    : dataset.py
@Software: PyCharm
@Describe: 
'''
import os
import numpy as np
import torch
import matplotlib.pyplot as plt
from torch.utils.data import Dataset, DataLoader

_BASE_DIR = os.path.dirname(os.path.abspath(__file__))


class Yolo_Dataset(Dataset):
	def __init__(self, txt_path, S=7, B=2, C=1):
		super(Yolo_Dataset, self).__init__()
		self.S = S
		self.B = B
		self.C = C
		f = open(txt_path, 'r')
		truth = {}
		for line in f.readlines():
			data = line.split(" ")
			truth[os.path.join(_BASE_DIR, 'data', data[0])] = []
			for i in data[1:]:
				truth[os.path.join(_BASE_DIR, 'data', data[0])].append(
					[float(j) if float(j) != int(float(j)) else int(j) for j in i.split(',')])
		self.truth = truth
		self.signals = list(self.truth.keys())

	def __getitem__(self, idx):
		sig_path = self.signals[idx]
		signal = np.load(sig_path, allow_pickle=True)
		assert signal['FS'] == 8, '采样率必须为8Hz'
		if 'UCD' in sig_path:
			mean_value = -0.000130507509104628
			std_value = 0.8905973492299032
		else:
			mean_value = 0
			std_value = 1
		resp = signal['SUM']
		# plt.plot(resp)
		# plt.show()
		boxes = self.truth[sig_path]

		# Convert to cell
		# shape [N, S, 7] (cls, p, xc, w, 0, 0, 0)
		label_matrix = torch.zeros((self.S, self.C + self.B * (2 + 1)))
		for box in boxes:
			cls, xc, w = box
			# 转换为第i个cell的中心点相对坐标
			i = int(self.S * xc)  # 第i个cell
			xc_cell = self.S * xc - i

			# 转换为第i个cell的宽度的相对长度
			w_cell = self.S * w

			if label_matrix[i, 1] == 0:
				label_matrix[i, 1] = 1
				label_matrix[i, 2:4] = torch.tensor([xc_cell, w_cell])
				label_matrix[i, 0] = cls
		resp = torch.tensor(resp, dtype=torch.float32)
		# Normalization
		resp = (resp - mean_value) / std_value
		return (resp, label_matrix)

	def __len__(self):
		return len(self.truth)


if __name__ == '__main__':
	yolo_dataset = Yolo_Dataset(f'./data/train.txt')
	dataloader = DataLoader(yolo_dataset, batch_size=4, shuffle=False)
	for target, resp in dataloader:
		plt.plot(resp[0])
		plt.show()
